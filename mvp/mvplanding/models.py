from django.db import models
from django.utils.translation import ugettext_lazy as _

class Header(models.Model):
    logo = models.FileField(upload_to='images/', blank=True)
    site_name = models.CharField(max_length=100, blank=True,default='')
    top_text = models.CharField(max_length=100, blank=True,default='')

    def __str__(self):
        return self.site_name

class HeroBanner(models.Model):
    hero_header = models.CharField(max_length=200,blank=True,default='')
    background_image = models.FileField(upload_to='images/',blank=True)
    hero_text = models.CharField(max_length=500, blank=True, default='')

    def __str__(self):
        return self.hero_header

class MainContent(models.Model):
    title = models.CharField(max_length=200,blank=True,default='')
    content = models.TextField(blank=True, default='')
    main_image = models.FileField(upload_to='images/',blank=True)

    def __str__(self):
        return self.title

class Footer(models.Model):
    copy_right = models.CharField(max_length=200,blank=True,default='')
    footer_text = models.CharField(max_length=200,blank=True,default='')

    def __str__(self):
        return self.copy_right

class Page(models.Model):
    title=models.CharField(max_length=200,blank=True,default='')
    header=models.ForeignKey(Header,blank=True, null=True, related_name='header')
    hero_banner=models.ForeignKey(HeroBanner, blank=True, null=True,related_name='hero_banner')
    main_content = models.ForeignKey(MainContent, blank=True, null=True,related_name='main_content')
    footer = models.ForeignKey(Footer, blank=True, null=True,related_name='footer')

    def __str__(self):
        return self.title
