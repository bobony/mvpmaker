from rest_framework import serializers

from .models import Header, HeroBanner, MainContent, Footer, Page

class HeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Header
        fields = ('logo', 'site_name', 'top_text')

class HeroBannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = HeroBanner
        fields =('hero_header', 'background_image', 'hero_text')

class MainContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = MainContent
        fields =('title','content','main_image')

class FooterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Footer
        fields=('copy_right','footer_text')

class  PageSerializer(serializers.ModelSerializer):
    header = HeaderSerializer()
    hero_banner = HeroBannerSerializer()
    main_content = MainContentSerializer()
    footer = FooterSerializer()
    class Meta:
        model=Page
        fields = ('title', 'header', 'hero_banner', 'main_content', 'footer')
