from django.contrib import admin

from .models import Header, HeroBanner, MainContent, Footer, Page

admin.site.register(Header)
admin.site.register(HeroBanner)
admin.site.register(MainContent)
admin.site.register(Footer)
admin.site.register(Page)
